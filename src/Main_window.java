import javax.swing.*;//elementos de la UI
import java.awt.*;//propiedades de la UI

class Main_window extends JFrame{

    /*************
     * Atributos
     ************/
    private JLabel lblContratarProfe;
    private JLabel lblNombre;
    private JLabel lblSalario;
    //Cajas de texto
    private JTextField txtNombre;
    private JTextField txtSalario;
    //Botones
    private JButton btnContratar;
    private JButton btnLiquidarNom;
    private JButton btnLiquidarPresSeg;
    private JButton btnCurso;

    public Main_window(){
        //Configuracion de la ventana
        setTitle("Main_window");
        setBounds(0, 0, 700, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        //GridLayout esquema = new GridLayout(1, 2, 10, 20);
        getContentPane().setLayout(null);

        //Panel contratar profesor
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(new Color(204, 204, 204));
        lblContratarProfe = new JLabel("Contratar Profesor");
        panel.add(lblContratarProfe, BorderLayout.NORTH);

        //Panel contenedor del formulario central del panel 'Contratar profesor'
        JPanel formulario = new JPanel(new GridLayout(4, 1, 5, 5));
        formulario.setBackground(new Color(204, 204, 204));
        lblNombre = new JLabel("Nombre");
        formulario.add(lblNombre);
        txtNombre = new JTextField(12);
        formulario.add(txtNombre);
        lblSalario = new JLabel("Salario");
        formulario.add(lblSalario);
        txtSalario = new JTextField(12);
        formulario.add(txtSalario);

        panel.add(formulario, BorderLayout.CENTER);

        btnContratar = new JButton("Contratar");
        panel.add( new JPanel().add(btnContratar), BorderLayout.SOUTH );
        panel.setBounds(40, 40, 200, 200);

        //Panel contenedor de las opciones
        JPanel panelOpciones = new JPanel( new GridLayout(3, 1, 10, 10));
        panelOpciones.setBounds(360, 40, 300, 200);
        //Inicializar los botones
        btnLiquidarNom = new JButton("Liquidación de nómina");
        panelOpciones.add(btnLiquidarNom);

        btnLiquidarPresSeg = new JButton("Liquidación prestaciones y seguridad social");
        panelOpciones.add(btnLiquidarPresSeg);

        btnCurso = new JButton("Agregar curso a un profesor");
        panelOpciones.add(btnCurso);

        //añade el panel a la ventana principal
        add(panel);
        add(panelOpciones);
    }



}